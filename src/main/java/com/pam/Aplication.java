package com.pam;

public class Aplication {

    public static void main(String[] args) {
        Phone myAndroid = new Android("11122","4.5","2017","one non-movable section");
        System.out.println(myAndroid);

      Phone myIPhone = new IPhone("22000","5.5","2019","movable section");
        System.out.println(myIPhone);

        print(myAndroid);
        print(myIPhone);

    }

    public  static  void print(Phone phone) {
        System.out.println(phone);
        System.out.println(phone.buzz());
    }
}
