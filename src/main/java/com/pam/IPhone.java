package com.pam;

public class IPhone extends Phone {

    private String version;

    @Override
    public String buzz() {
        return "bymbym";
    }

    @Override
    public String toString() {
        return "IPhone{" + super.toString()+
                "version='" + version + '\'' +
                '}';
    }

    public IPhone(String price, String weight, String year, String version) {
        super(price, weight, year);
        this.version = version;


    }
}
