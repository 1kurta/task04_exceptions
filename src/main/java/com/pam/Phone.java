package com.pam;

public abstract class  Phone  {
    private String  price;
    private String weight;
    private String year;

    public Phone(String price,String  weight,String year) {
        this. price =  price;
        this.weight= weight;
        this.year = year;
    }


    public abstract String buzz();

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "price='" + price + '\'' +
                ", weight='" + weight + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
