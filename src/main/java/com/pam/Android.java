package com.pam;

public class  Android extends Phone {
    private String style;


    public Android(String price, String weight, String year, String style) {
        super(price, weight, year);
        this.style = style;
    }

    @Override
    public String buzz() {
        return "bimbim";
    }

    @Override
    public String toString() {
        return "Android{" + super.toString()+
                "style=" + style +
                '}';
    }
}
